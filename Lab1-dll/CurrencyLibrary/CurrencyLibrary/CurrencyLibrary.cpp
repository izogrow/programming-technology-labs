//#define LIB_EXPORT
#include "pch.h"
#include <utility>
#include <limits.h>
#include "CurrencyLibrary.h"
#include <iostream>

using namespace std;

int conversion(double money, char currency_of, char currency_to)
{
	system("color 02");
	double rub = 1, euro = 91.916, dollar = 78.171, hryvnia = 27.5894, lira = 7.8826;
	cout << "Total: ";
	switch (currency_of) {
	case 'r': case 'R': {
		switch (currency_to) {
		case 'r': case 'R': {
			cout << money * rub << " r";
			break;
		}
		case 'e': case 'E': {
			cout << money / euro << " e";
			break;
		}
		case 'd': case 'D': {
			cout << money / dollar << " d";
			break;
		}
		case 'h': case 'H': {
			cout << money / hryvnia << " h";
			break;
		}
		case 'l': case 'L': {
			cout << money / lira << " l";
			break;
		}
		}
		break;
	}
	case 'e': case 'E': {
		switch (currency_to) {
		case 'r': case 'R': {
			cout << money * euro << " r";
			break;
		}
		case 'e': case 'E': {
			cout << money << " e";
			break;
		}
		case 'd': case 'D': {
			cout << money * euro / dollar << " d";
			break;
		}
		case 'h': case 'H': {
			cout << money * euro / hryvnia << " h";
			break;
		}
		case 'l': case 'L': {
			cout << money * euro / lira << " l";
			break;
		}
		}
		break;
	}
	case 'd': case 'D': {
		switch (currency_to) {
		case 'r': case 'R': {
			cout << money * dollar << " r";
			break;
		}
		case 'e': case 'E': {
			cout << money * dollar / euro << " e";
			break;
		}
		case 'd': case 'D': {
			cout << money << " d";
			break;
		}
		case 'h': case 'H': {
			cout << money * dollar / hryvnia << " h";
			break;
		}
		case 'l': case 'L': {
			cout << money * dollar / lira << " l";
			break;
		}
		}
		break;
	}
	case 'g': case 'G': {
		switch (currency_to) {
		case 'r': case 'R': {
			cout << money * hryvnia << " r";
			break;
		}
		case 'e': case 'E': {
			cout << money * hryvnia / euro << " e";
			break;
		}
		case 'd': case 'D': {
			cout << money * hryvnia / dollar << " d";
			break;
		}
		case 'h': case 'H': {
			cout << money << " h";
			break;
		}
		case 'l': case 'L': {
			cout << money * hryvnia / lira << " l";
			break;
		}
		}
		break;
	}
	case 'l': case 'L': {
		switch (currency_to) {
		case 'r': case 'R': {
			cout << money * lira << " r";
			break;
		}
		case 'e': case 'E': {
			cout << money * lira / euro << " e";
			break;
		}
		case 'd': case 'D': {
			cout << money * lira / dollar << " d";
			break;
		}
		case 'h': case 'H': {
			cout << money * lira / hryvnia << " h";
			break;
		}
		case 'l': case 'L': {
			cout << money << " l";
			break;
		}
		}
		break;
	}
	}

	return 0;
}

