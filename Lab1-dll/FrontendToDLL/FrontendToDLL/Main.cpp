#include <iostream>
#include "CurrencyLibrary.h"
using namespace std;

int main()
{
	system("color 02");
	cout << "Currency conversion" << endl;
	cout << "Values: \n Russian Ruble(r) = 1 \n Euro(e) = 91.916 \n Dollar(d) = 78.171 \n Hryvnia(h) = 27.5894 \n Lyre(l) = 7.8826" << endl;

	double money;
	char currency_of, currency_to;
	char answer;
	cout << "Example: 11232 d l" << endl;
	do {

		cin >> money >> currency_of >> currency_to;
		conversion(money, currency_of, currency_to);
		cout << "\nmore? (Y/N)";
		cin >> answer;
	} while (answer == 'Y' || answer == 'y');
	cout << "Lab 1 / Shared Libraries / Programming Technologies" << endl;
	return 0;
}