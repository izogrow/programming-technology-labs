# -*- coding: utf-8 -*-
import json

END_CHARACTER = "\0"
MESSAGE_PATTERN = "{username}>{message}"
TARGET_ENCODING = "utf-8"
NUMBER_MESSAGE_PATTERN = "{message}"
USERNAME_MESSAGE_PATTERN = "{username}"


class Message(object):
    def __init__(self, **kwargs):
        self.username = None
        self.message = None
        self.quit = False
        self.__dict__.update(kwargs)

    def __str__(self):
        return MESSAGE_PATTERN.format(**self.__dict__)

    def marshal(self):
        return (json.dumps(self.__dict__) + END_CHARACTER).encode(TARGET_ENCODING)

    def get_message(self):
        return NUMBER_MESSAGE_PATTERN.format(**self.__dict__)

    def get_client(self):
        return USERNAME_MESSAGE_PATTERN.format(**self.__dict__)

    def marshal_win(self):
        # print(self.__dict__)
        self.message = "you win!"
        return self.marshal()

    def marshal_lose(self):
        self.message = "you lose! please try again"
        return self.marshal()

    def marshal_err(self):
        self.message = "you can't override your own number"
        return self.marshal()

    def marshal_save(self):
        self.message = "session has successfully saved!"
        return self.marshal()
