import json

a = json.dumps({'first': 'it\'s me!', 'second': 'it\'s also me!'}, indent=4) # indent=4 is enabling pretty printing
print(a)
b = json.dumps([1, 2, {'first': 'it\'s me!'}], indent=4)
print(b)

with open('test_json.txt', 'w') as file:
    file.write(a)

with open('test_json.txt', 'r') as file:
    c = json.loads(file.read())
    print("c = ", c)
